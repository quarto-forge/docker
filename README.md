# Docker Images with Quarto

TL;DR https://gitlab.com/quarto-forge/docker/container_registry/

[Quarto](https://quarto.org/)® is an open-source scientific and technical publishing system built on [Pandoc](https://pandoc.org/).

We provide three tiers of [Docker](https://www.docker.com/) images:

- **Tier 0**: vanilla GNU/Linux Docker image with Quarto
- **Tier 1**: GNU/Linux Docker image with Quarto and programming languages
- **Tier 2**: GNU/Linux Docker image with Quarto, programming languages, and **one** web IDE

## Tier 0

Docker images in this tier are the best option to GitLab CI if you are not doing any computation.

- `registry.gitlab.com/quarto-forge/docker/quarto` is a minimalist image with vanilla Quarto and the best option to if you only want to render your document to HTML.
- `registry.gitlab.com/quarto-forge/docker/quarto_all` includes a copy of LaTeX and Libreoffice Writer in addition to Quarto. This is the base image for Tier 1.

`.gitlab-ci.yml` example:

```
image: registry.gitlab.com/quarto-forge/docker/quarto

pages:
  script:
    - quarto render --execute --to html --output-dir public
  artifacts:
    paths:
      - public
```

## Tier 1

Docker images in this tier are the best option to GitLab CI if you are doing any computation.

### Python

In addition to Quarto, LaTeX, and LibreOffice, `registry.gitlab.com/quarto-forge/docker/python` includes popular Python packages ([list of Python packages included](python.env.yaml)).
**This image only supports jupyter as the engine.**

`.gitlab-ci.yml` example:

```
image: registry.gitlab.com/quarto-forge/docker/python

pages:
  script:
    - quarto render --execute --to html --output-dir public
  artifacts:
    paths:
      - public
```

### R

In addition to Quarto, LaTeX, and LibreOffice, `registry.gitlab.com/quarto-forge/docker/r` includes popular R packages ([list of R packages included](r.env.yaml)).
**This image only supports knitr as the engine.**

`.gitlab-ci.yml` example:

```
image: registry.gitlab.com/quarto-forge/docker/rstats

pages:
  script:
    - quarto render --execute --to html --output-dir public
  artifacts:
    paths:
      - public
```

### All Languages

In addition to Quarto, LaTeX, and LibreOffice, `registry.gitlab.com/quarto-forge/docker/polyglot` includes popular Python and R packages ([list of Python and R packages included](polyglot.env.yaml)).
This image supports both jupyter and knitr as the engine and includes iPython and iR kernel plus reticulate.
This is the base image used for Tier 2.

`.gitlab-ci.yml` example:

```
image: registry.gitlab.com/quarto-forge/docker/polyglot

pages:
  script:
    - quarto render --execute --to html --output-dir public
  artifacts:
    paths:
      - public
```

## Tier 2

Docker images in this tier are the best option to use when writing your document.

### Jupyter Lab

In addition to Quarto, LaTeX, LibreOffice, Python, and R, it includes Jupyter Lab.

[Example of docker-compose.yml](docker-compose/jupyter.yml)

### RStudio Server

In addition to Quarto, LaTeX, LibreOffice, Python, and R, it includes RStudio Server.

[Example of docker-compose.yml](docker-compose/rstudio.yml)